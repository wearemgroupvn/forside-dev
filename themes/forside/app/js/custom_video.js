! function(e) {

	e.fn.videoPlayer = function(i) {
        var t = {
            playerWidth: "0.95",
            videoClass: "video"
        };
        return i && e.extend(t, i), this.each(function() {
            e(this)[0].addEventListener("loadedmetadata", function() {
                var i = e(this),
                    a = t;
                i.wrap('<div class="' + a.videoClass + '"></div>');
                var s = i.parent("." + a.videoClass);
                e('<div class="player"><div class="play-pause play"><span class="play-button">&#9658;</span><div class="pause-button"><span> </span><span> </span></div></div><div class="progress"><div class="progress-bar"><div class="button-holder"><div class="progress-button"> </div></div></div><div class="time"><span class="ctime">00:00</span><span class="stime"> / </span><span class="ttime">00:00</span></div></div><div class="volume"><div class="volume-holder"><div class="volume-bar-holder"><div class="volume-bar"><div class="volume-button-holder"><div class="volume-button"> </div></div></div></div></div><div class="volume-icon v-change-0"><span> </span></div></div><div class="fullscreen"> <a href="#"> </a></div></div>').appendTo(s), $videoWidth = i.width(), s.width($videoWidth + "px"), s.find(".player").css({
                    width: 100 * a.playerWidth + "%",
                    left: (100 - 100 * a.playerWidth) / 2 + "%"
                });
                var n, o, d = e(this)[0],
                    r = d.duration,
                    l = d.volume,
                    u = !1,
                    c = !1,
                    h = !1,
                    m = !1,
                    v = !1,
                    f = 0,
                    p = 0,
                    b = 0,
                    g = 0,
                    l = d.volume;
                s.bind("selectstart", function() {
                    return !1
                });
                var C = s.find(".progress").width(),
                    w = function() {
                        var i = d.buffered;
                        if (s.find("[class^=buffered]").remove(), i.length > 0)
                            for (var t = i.length; t--;) {
                                $maxBuffer = i.end(t), $minBuffer = i.start(t);
                                var a = $minBuffer / r * 100,
                                    n = ($maxBuffer - $minBuffer) / r * 100;
                                e('<div class="buffered"></div>').css({
                                    left: a + "%",
                                    width: n + "%"
                                }).appendTo(s.find(".progress"))
                            }
                    };
                w();
                var y = function(i) {
                    var t = Math.round(e(".progress-bar").width() / C * r),
                        a = d.currentTime,
                        n = 0,
                        o = Math.floor(t / 60),
                        l = Math.round(r / 60),
                        u = Math.round(r - 60 * l);
                    t && (n = Math.round(t) - 60 * o, n > 59 && (n = Math.round(t) - 60 * o, 60 == n && (o = Math.round(t / 60), n = 0))), b = a / r * C, n < 10 && (n = "0" + n), u < 10 && (u = "0" + u), 1 != i && (s.find(".progress-bar").css({
                        width: b + s.find(".progress-button").width() + "px"
                    }), s.find(".progress-button").css({
                        left: b - s.find(".progress-button").width() + "px"
                    })), s.find(".ctime").html(o + ":" + n), s.find(".ttime").html(l + ":" + u), d.currentTime > 0 && 0 == d.paused && 0 == d.ended && w()
                };
                y(), d.addEventListener("timeupdate", y), s.find(".play-pause").bind("click", function() {
                    m = !(d.currentTime > 0 && 0 == d.paused && 0 == d.ended), 0 == m ? (d.pause(), e(this).addClass("play").removeClass("pause"), w()) : (v = !0, d.play(), e(this).addClass("pause").removeClass("play"))
                }), s.find(".progress").bind("mousedown", function(e) {
                    u = !0, 1 == m && d.pause(), f = e.pageX - s.find(".progress").offset().left, n = f / C * r, d.currentTime = n
                }), s.find(".volume-bar-holder").bind("mousedown", function(i) {
                    return c = !0, p = s.find(".volume-bar-holder").height() - (i.pageY - s.find(".volume-bar-holder").offset().top), p < 0 || p > e(this).height() ? (c = !1, !1) : (s.find(".volume-bar").css({
                        height: p + "px"
                    }), s.find(".volume-button").css({
                        top: p - s.find(".volume-button").height() / 2 + "px"
                    }), d.volume = s.find(".volume-bar").height() / e(this).height(), o = s.find(".volume-bar").height() / e(this).height(), l = s.find(".volume-bar").height() / e(this).height(), void k())
                });
                var k = function() {
                    for (var e = 0; e < 1; e += .1) {
                        var i = parseInt(Math.floor(10 * e)) / 10,
                            t = 10 * i + 1;
                        1 == l ? 1 == h ? s.find(".volume-icon").removeClass().addClass("volume-icon volume-icon-hover v-change-11") : s.find(".volume-icon").removeClass().addClass("volume-icon v-change-11") : 0 == l ? 1 == h ? s.find(".volume-icon").removeClass().addClass("volume-icon volume-icon-hover v-change-1") : s.find(".volume-icon").removeClass().addClass("volume-icon v-change-1") : l > i - .1 && g < i && !s.find(".volume-icon").hasClass("v-change-" + t) && (1 == h ? s.find(".volume-icon").removeClass().addClass("volume-icon volume-icon-hover v-change-" + t) : s.find(".volume-icon").removeClass().addClass("volume-icon v-change-" + t))
                    }
                };
                k(), s.find(".volume").hover(function() {
                    h = !0
                }, function() {
                    h = !1
                }), e("body, html").bind("mousemove", function(i) {
                    if (1 == v && s.hover(function() {
                            s.find(".player").stop(!0, !1).animate({
                                opacity: "1"
                            }, .5)
                        }, function() {
                            s.find(".player").stop(!0, !1).animate({
                                opacity: "1"
                            }, .5)
                        }), 1 == u) {
                        $draggingProgress = !0;
                        var t = 0,
                            a = s.find(".progress-button").width();
                        f = i.pageX - s.find(".progress").offset().left, 1 == m && n < r && s.find(".play-pause").addClass("pause").removeClass("play"), f < 0 ? (t = 0, d.currentTime = 0) : f > C ? (d.currentTime = r, t = C) : (t = f, n = f / C * r, d.currentTime = n), s.find(".progress-bar").css({
                            width: $progMove + "px"
                        }), s.find(".progress-button").css({
                            left: $progMove - a + "px"
                        })
                    }
                    if (1 == c) {
                        p = s.find(".volume-bar-holder").height() - (i.pageY - s.find(".volume-bar-holder").offset().top);
                        var b = 0;
                        if ("none" == s.find(".volume-holder").css("display")) return c = !1, !1;
                        s.find(".volume-icon").hasClass("volume-icon-hover") || s.find(".volume-icon").addClass("volume-icon-hover"), p < 0 || 0 == p ? (l = 0, b = 0, s.find(".volume-icon").removeClass().addClass("volume-icon volume-icon-hover v-change-11")) : p > e(this).find(".volume-bar-holder").height() || p / s.find(".volume-bar-holder").height() == 1 ? (l = 1, b = s.find(".volume-bar-holder").height(), s.find(".volume-icon").removeClass().addClass("volume-icon volume-icon-hover v-change-1")) : (l = s.find(".volume-bar").height() / s.find(".volume-bar-holder").height(), b = p), s.find(".volume-bar").css({
                            height: b + "px"
                        }), s.find(".volume-button").css({
                            top: b + s.find(".volume-button").height() + "px"
                        }), k(), d.volume = l, o = l
                    }
                    0 == h ? (s.find(".volume-holder").stop(!0, !1).fadeOut(100), s.find(".volume-icon").removeClass("volume-icon-hover")) : (s.find(".volume-icon").addClass("volume-icon-hover"), s.find(".volume-holder").fadeIn(100))
                }), d.addEventListener("ended", function() {
                    m = !1, 0 == $draggingProgress && s.find(".play-pause").addClass("play").removeClass("pause")
                }), s.find(".volume-icon").bind("mousedown", function() {
                    l = d.volume, "undefined" == typeof o && (o = d.volume), l > 0 ? (d.volume = 0, l = 0, s.find(".volume-bar").css({
                        height: "0"
                    }), k()) : (d.volume = o, l = o, s.find(".volume-bar").css({
                        height: 100 * o + "%"
                    }), k())
                }), e("body, html").bind("mouseup", function(e) {
                    u = !1, c = !1, $draggingProgress = !1, 1 == m && d.play(), w()
                }), d.requestFullscreen || d.mozRequestFullScreen || d.webkitRequestFullScreen || e(".fullscreen").hide(), e(".fullscreen").click(function() {
                    d.requestFullscreen ? d.requestFullscreen() : d.mozRequestFullScreen ? d.mozRequestFullScreen() : d.webkitRequestFullScreen && d.webkitRequestFullScreen()
                })
            })
        })
    }
	
}(jQuery), jQuery(document).ready(function(e) {
	e("video").videoPlayer({
        playerWidth: 1,
        videoClass: "video"
    });
});
