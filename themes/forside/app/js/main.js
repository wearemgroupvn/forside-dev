jQuery(document).ready(function(e) {
	var currentId = $('.current-id').text();
	var currentLanguage = $('html').attr('lang');
	// View pager top
   
	var viewPager = $('.view-list-hyheder-article nav.pager-nav').html();
	$('.view-list-hyheder-article .view-filters').after(viewPager);

    if($('.view-list-hyheder-article-for-en .pager-nav') != '') {
    	$('.view-list-hyheder-article-for-en .view-filters').after($('.view-list-hyheder-article-for-en .pager-nav').html());
    }

    $("div.view-list-hyheder-article, div.view-list-hyheder-article-for-en").find("form.views-exposed-form").find("select").bind("change", function () {
	  $(this).closest("form").trigger("submit");
	}).end().find("input[type='submit']").addClass("visually-hidden");
    
	$('.menu-open').click(function() {

		$('#navbar-collapse').css({
			'display': 'block'
		});
		$('#navbar-collapse').animate({left: '0px'}, 200);

		$('.menu-close').click(function(){
			$('#navbar-collapse').css({left: '-296px'}, 100);
		});
	});

	
	var dataId = $('.menu-sidebar-item-'+currentId).attr('dataid');
	var parentId = $('.menu-sidebar-item-'+currentId).attr('parentid');
	var getElementParent = $('.menu-sidebar-item-'+currentId).parent().parent().parent();
	var childId = getElementParent.nextAll();
	


	if(parentId != '') {
		$('.menu-sidebar-item-'+currentId).addClass('has-parent');
	}

	childId.each(function(index, el) {
		if($(this).find('.menu-sidebar-item').attr('parentid') != '' && $(this).find('.menu-sidebar-item').attr('parentid') == currentId) {
			$(this).find('.menu-sidebar-item').addClass('has-parent');
		}
	});

	$('.menu-sidebar-item-'+parentId).addClass('has-parent');

	getElementParent.prevAll().each(function(index, el) {
		if($(this).find('.menu-sidebar-item').attr('parentid') != '' && $(this).find('.menu-sidebar-item').attr('parentid') == parentId) {
			$(this).find('.menu-sidebar-item').addClass('has-parent');
		}
	});

	$('.menu-sidebar-item').each(function(index, el) {
		if($(this).attr('parentid') == ($('.menu-sidebar-item-'+parentId).attr('parentid'))) {
			$(this).addClass('has-parent');
		}
		if($(this).attr('parentid') == ($('.menu-sidebar-item-'+currentId).attr('parentid'))) {
			$(this).addClass('has-parent');
		}

		if((currentLanguage == 'en') && ($(this).attr('notshow') == 1)) {
			$(this).remove();
		}

		if(currentLanguage == 'da') {
			if($(this).attr('dataid') == 346 || $(this).attr('dataid') == 347) {
				$(this).remove();
			}
		}
	});


	$('.menu-sidebar-item').each(function(index, el) {
		if($(this).attr('parentid') == '') {
			$(this).addClass('not-child');
		}
	});

	if(currentId == dataId) {
		$('.menu-sidebar-item-'+currentId).addClass('active');
	}

	$('.menu-sidebar-item').each(function(index, el) {
		if($(this).attr('parentid') == ($('.menu-sidebar-item-'+currentId+':not(.not-child)').attr('dataid'))) {
			$(this).addClass('level2');
		}
		if($(this).attr('parentid') == $('.menu-sidebar-item-'+$(this).attr("parentid")+':not(.not-child)').attr('dataid')) {
			$('.menu-sidebar-item-'+$(this).attr("parentid")+':not(.not-child)').addClass('level1');
			$(this).addClass('level2');
		}
		if($(this).attr('parentid') == $('.menu-sidebar-item-'+$(this).attr("parentid")+'.not-child').attr('dataid')) {
			$(this).addClass('level1');
		}
	});

	if($('.sidebar-menu .menu-sidebar-item').length == 1) {
		$('.menu-sidebar-item').removeClass('level1');
		$('.menu-sidebar-item').removeClass('level2');
	}


	if($('article.page').attr('data-history-node-id') == 422) {
		if(currentLanguage == 'da') {
			$('.view-list-hyheder-article').show();
			sessionStorage.removeItem('key');
		}
		if(currentLanguage == 'en') {
			$('.allpost').show();
			$('.view-list-hyheder-article-for-en').show();

			$(".allpost").click(function() {
				if(this.checked) {
					setTimeout(function(){
						sessionStorage.setItem('key', 'value');
						$('.view-list-hyheder-article-for-en').hide();
						$('.view-list-hyheder-article-for-all').show();
					}, 1000);
				}else {
					setTimeout(function(){
						sessionStorage.removeItem('key');
						$('.view-list-hyheder-article-for-all').hide();
						$('.view-list-hyheder-article-for-en').show();
					}, 1000);
				}
			});

			if(($(".allpost:checked").length == 0) && (sessionStorage.getItem('key') == null)) {
				$(".allpost").attr('checked', false);
				$('.view-list-hyheder-article-for-all').hide();
				$('.view-list-hyheder-article-for-en').show();
			}else {
				$(".allpost").attr('checked', true);
				$('.view-list-hyheder-article-for-en').hide();
				$('.view-list-hyheder-article-for-all').show();
			}

		}
	}else {
		$('.show-all-post').remove();
		$('.view-list-hyheder-article').hide();
		$('.view-list-hyheder-article-for-en').hide();
		$('.view-list-hyheder-article-for-all').hide();
		$('.allpost').hide();
	}

	$('.LatestNewsResultList').next().hide();
	$('.LatestNewsResultList').prev().hide();

	$('.view-latest-news .views-field-title a').addClass('LatestNewsListItemAnchor');

    // Resize windown and search for desktop and mobile

    $('.search-block-form .input-group-btn').attr('id','btn-search-icon');
    $('.search-block-form .input-group-btn').click(function() {
    	$('.search-block-form input.form-search').css('opacity', 1);
    	$('.block-search-form-block form .input-group button').show();
    });

    $(document).click(function(e) {
		if($(window).width() <= 991) {
			if( e.target.id != 'btn-search-icon' && e.target.id != 'edit-keys') {
				$('.search-block-form input.form-search').css('opacity', 0);
				$('.block-search-form-block form .input-group button').hide();
			}

			if( e.target.id != 'navbar-collapse' && e.target.id != 'open-menu' && e.target.id != 'icon-arrow') {
				$('#navbar-collapse').css({left: '-296px'}, 100);
			}
		}
	});

	$(window).resize(function() {

		if($(window).width() > 991) {
			$('.search-block-form input.form-search').css('opacity', 1);
			$('.block-search-form-block form .input-group button').show();
		}else {
			$('.search-block-form input.form-search').css('opacity', 0);
			$('.block-search-form-block form .input-group button').hide();
		}
	});

	// End resize windown and search for desktop and mobile
	function NyVidensbaseSearch() {
		window.open(
		  'https://vidensbasen.anklagemyndigheden.dk/Soeg?term=' + encodeURIComponent($('#NyVidensbaseSrch').val()),
		  '_blank'
		);
	}

	$('#NyVidensbaseSrch').on('keyup', function (e) {
	    if (e.keyCode == 13) {
	        NyVidensbaseSearch();
	    }
	});

	$('#NyVidensbaseSearch').click(function() {
		NyVidensbaseSearch();
	});

	var heightTitle = $('.forsite-article .content .content-top .bg-black').outerHeight(true);

	$('.forsite-article .content .date-and-author-has-title-body').css('top', (parseInt(heightTitle+35))+'px');

	if((currentLanguage == 'en')) {

		$('.language-switcher-language-url ul.links li').each(function(index, el) {
			if($(this).attr('hreflang') == 'da') {
				$(this).find('.language-link').text('Dansk');
			}
		});
	}

	$('.language-switcher-language-url ul.links li').each(function(index, el) {
		if($(this).attr('hreflang') == currentLanguage) {
			$(this).hide();
		}else {
			$(this).show();
		}
	});


    // Active menu

    var getTermid = $('header .navbar-header').attr('term-id');

    $('.navbar-header .menu.nav li').each(function(index, el) {
    	if(getTermid == 12 || getTermid == 13) {
    		if(index == 1) {
    			$(this).addClass('active');
    			$(this).find('a').addClass('is-active');
    		}
    	}

    	if(getTermid == 2) {
    		if(index == 2) {
    			$(this).addClass('active');
    			$(this).find('a').addClass('is-active');
    		}
    	}

    	if(getTermid == 3) {
    		if(index == 3) {
    			$(this).addClass('active');
    			$(this).find('a').addClass('is-active');
    		}
    	}

    	if(getTermid == 4) {
    		if(index == 4) {
    			$(this).addClass('active');
    			$(this).find('a').addClass('is-active');
    		}
    	}

    	if(getTermid == 5) {
    		if(index == 5) {
    			$(this).addClass('active');
    			$(this).find('a').addClass('is-active');
    		}
    	}
    });

    $('#search-block-form, #search-form').submit(function(event) {
    	if($('#edit-keys').val() == '') {
    		event.preventDefault();
    	}
    });

    $('.has-submenu').click(function() {
    	$(this).find('.submenu').toggle(400);
    	$(this).find('#icon-arrow').toggleClass('open-submenu');
    });

    var hasTitleInBodyField = $('.forsite-article .content .field--name-body .bg-black:first-child').text();

    if(hasTitleInBodyField && hasTitleInBodyField != 'Seneste nyheder') {
    	$('.forsite-article .content .field-title').remove();
    	$('.forsite-article .content .date-and-author-not-has-title-body').remove();
    } else {
    	$('.forsite-article .content .date-and-author-has-title-body').remove();
    }

    $('.forsite-article .content .field--name-body .NewsManchetLine1 .NewsTools, .forsite-article .content .field--name-body .NewsManchetLine2').remove();
    

    $('.sidebar-menu .dropdown .dropdown-menu li, .group-menu-mobile .dropdown .dropdown-menu li').each(function(index, el) {
    	if($(this).hasClass('active')) {
    		$(this).parent().parent().addClass('not-hover');
    	}
    });

    $('.sidebar-menu .navbar-nav li.dropdown, .group-menu-mobile .navbar-nav li.dropdown').each(function(index, el) {
    	$(this).find('a').removeAttr('data-toggle');
    });

    // Add Cookie For Cookie kontrol

	function PopUp(hideOrshow) {
        if (hideOrshow == 'hide') {
            $('.CookieAcceptImageDivClass').hide();
        }
        else if(localStorage.getItem("popupWasShown") == null) {
            localStorage.setItem("popupWasShown",1);
            $('.CookieAcceptImageDivClass').show();
        }
    }
    $(window).load(function() {
        setTimeout(function () {
            PopUp('show');
        }, 1500);
    });

    $('.CookieAcceptDivCloseButtonClass, .CookieAcceptDivAnchorClass').click(function() {
    	PopUp('hide');
    });

    // End Cookie For Cookie kontrol

});

// maps feature

jQuery(document).ready(function($) {

	$("#PrimaryCard").addClass("hide");
	$("#SecondaryCard").addClass("hide");

	// right menu mouse over
	$("ul.OrgChartList div").mouseover(function(){

		$("#PrimaryCard").removeClass("hide");
		$("#SecondaryCard").removeClass("hide");


			var selected_key = $(this).attr('id').slice(6);
	    var symbol_key = "#symbol" + selected_key + " > g";

	    var primary_card_data = branch_left[selected_key];
	    var secondary_card_data = branch_right[selected_key];


	    $(".land").removeClass("selected");
    	$("ul.OrgChartList div").removeClass("OrgChartHover");

    	$(this).addClass("OrgChartHover");

	    // map add class selected
	    $("ul.OrgChartList div").removeClass("selected");
	    $(symbol_key).addClass("selected");

	    // Fill data primary card

	    // If this element no parent
	    if ($("#field_pa_" + selected_key).text().length == 0) {
	    	// Fill out data to primary card
	    	$("div.StatsadvokatDivInfoHeadline").html($("#title_" + selected_key).html());
		    $("span.pri-street").html($("#field_street_" + selected_key).html());
		    $("span.pri-district").html($("#field_city_" + selected_key).html());
		    $("span.pri-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());
		    $("span.pri-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
		    if ($("#field_fax_" + selected_key).html().length <= 0){
		    	$("span.pri-fax").addClass('hide');
		    }
		    $("a.pri-email").html($("#field_email_" + selected_key).html());
		    $("a.pri-email").attr("href",$("#field_email_" + selected_key).html());
		    $("a.pri-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.pri-learn-more").attr("href",$("#field_underside_" + selected_key).html());

		    // Hide secondary card
		    $("#SecondaryCard").addClass("hide");

	    } else {// This has parent branch then show parent one

	      // If this element has parent
		    if ($("#field_pa_" + selected_key).text().length > 0) {
		    	// Fill out data to primary card
		    	var parent_key = $("#field_pa_" + selected_key).text();

		    	$("div.StatsadvokatDivInfoHeadline").html($("#title_" + parent_key).html());
			    $("span.pri-street").html($("#field_street_" + parent_key).html());
			    $("span.pri-district").html($("#field_city_" + parent_key).html());
			    $("span.pri-tel").html("Tlf: " + $("#field_phone_" + parent_key).html());
			    $("span.pri-fax").html("Fax: " + $("#field_fax_" + parent_key).html());
			    if ($("#field_fax_" + parent_key).html().length <= 0) {
			    	$("span.pri-fax").addClass("hide");
			    }

			    $("a.pri-email").html($("#field_email_" + parent_key).html());
			    $("a.pri-email").attr("href",$("#field_email_" + parent_key).html());
			    $("a.pri-learn-more").html('Læs mere om ' + $("#title_" + parent_key).html());
		    	$("a.pri-learn-more").attr("href",$("#field_underside_" + parent_key).html());

			    // Hide secondary card
			    $("#SecondaryCard").addClass("hide");
		    }

		    // Fill data secondary card
		    $("div.PolitiDivInfoHeadline").html($("#title_" + selected_key).html());
		    $("span.sec-street").html($("#field_street_" + selected_key).html());
		    $("span.sec-district").html($("#field_city_" + selected_key).html());
		    $("span.sec-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());

		    $("span.sec-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
		    if ($("#field_fax_" + selected_key).html().length <= 0) {
		    	$("span.sec-fax").addClass('hide');
		    }
		    $("a.sec-email").html($("#field_email_" + selected_key).html());
		    $("a.sec-email").attr("attr",$("#field_email_" + selected_key).html());
		    $("a.sec-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.sec-learn-more").attr("href",$("#field_underside_" + selected_key).html());

	    }



	    // Fill data secondary card
	    $("div.PolitiDivInfoHeadline").html($("#title_" + selected_key).html());
	    $("span.sec-street").html($("#field_street_" + selected_key).html());
	    $("span.sec-district").html($("#field_city_" + selected_key).html());
	    $("span.sec-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());
	    $("span.sec-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
	    if ($("#field_fax_" + selected_key).html().length <= 0) {
		    	$("span.sec-fax").addClass('hide');
		    }
	    $("a.sec-email").html($("#field_email_" + selected_key).html());
	    $("a.sec-email").attr("attr",$("#field_email_" + selected_key).html());
	     $("a.sec-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.sec-learn-more").attr("href",$("#field_underside_" + selected_key).html());


	    if ($.isEmptyObject(secondary_card_data)){
	    	$("#SecondaryCard").addClass("hide");
	    } else{
	    	$("#SecondaryCard").removeClass("hide");
	    }

		if(selected_key == 1 || selected_key == 3 || selected_key == 6) {
	    	$(".land").addClass("selected");
    } else {
			$(".land").removeClass("selected");
			$(symbol_key).addClass("selected");
    }

    if(selected_key == 4){
    		$("#symbol9 > g").addClass("selected");
    		$("#symbol10 > g").addClass("selected");
    		$("#symbol11 > g").addClass("selected");
    		$("#symbol12 > g").addClass("selected");
    		$("#symbol13 > g").addClass("selected");
    		$("#symbol14 > g").addClass("selected");
    }

    if(selected_key == 5){
    		$("#symbol15 > g").addClass("selected");
    		$("#symbol16 > g").addClass("selected");
    		$("#symbol17 > g").addClass("selected");
    		$("#symbol18 > g").addClass("selected");
    		$("#symbol19 > g").addClass("selected");
    		$("#symbol20 > g").addClass("selected");
    }


	});

	// Right menu mouse leave
	$("ul.OrgChartList div").mouseleave(function(){
		// $(this).removeClass("OrgChartHover");
		var selected_key = $(this).attr('id').slice(6);
	    var symboy_key = "#symbol" + selected_key + " > g";
	    // $(symboy_key).removeClass("selected");
	});

	// map mouse over
	$(".land").mouseenter(function(){
		$("#PrimaryCard").removeClass("hide");
		$("#SecondaryCard").removeClass("hide");

		// map
		$(".land").removeClass("selected");
			$(this).addClass("selected");

	 		var selected_key = $(this).parent().attr("id").slice(6);
	    var branch_key = "#branch" + selected_key;

	    // menu
	    $("ul.OrgChartList div").removeClass("OrgChartHover");
	    $(branch_key).addClass("OrgChartHover");

	    var primary_card_data = branch_left[selected_key];
	    var secondary_card_data = branch_right[selected_key];

	    // Fill data primary card

	    // If this element has no parent
	    if ($("#field_pa_" + selected_key).text().length == 0) {
	    	// Fill out data to primary card
	    	$("div.StatsadvokatDivInfoHeadline").html($("#title_" + selected_key).html());
		    $("span.pri-street").html($("#field_street_" + selected_key).html());
		    $("span.pri-district").html($("#field_city_" + selected_key).html());
		    $("span.pri-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());
		     $("span.pri-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
		    if ($("#field_fax_" + selected_key).html().length <= 0) {
		    	 $("span.pri-fax").addClass("hide");
		    }
		    $("span.pri-email").html($("#field_email_" + selected_key).html());
		    $("span.pri-email").attr("href",$("#field_email_" + selected_key).html());
		    $("a.pri-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.pri-learn-more").attr("href",$("#field_underside_" + selected_key).html());

		    // Hide secondary card
		    $("#SecondaryCard").addClass("hide");

	    } else {// This has parent branch then show parent one

	      // If this element has no parent
		    if ($("#field_pa_" + selected_key).text().length > 0) {
		    	// Fill out data to primary card
		    	var parent_key = $("#field_pa_" + selected_key).text();

		    	$("div.StatsadvokatDivInfoHeadline").html($("#title_" + parent_key).html());
			    $("span.pri-street").html($("#field_street_" + parent_key).html());
			    $("span.pri-district").html($("#field_city_" + parent_key).html());
			    $("span.pri-tel").html("Tlf: " + $("#field_phone_" + parent_key).html());
			     $("span.pri-fax").html("Fax: " + $("#field_fax_" + parent_key).html());
				   if ($("#field_fax_" + parent_key).html().length <= 0) {
				   	 $("span.pri-fax").addClass('hide');
				   }
			    $("span.pri-email").html($("#field_email_" + parent_key).html());
			    $("span.pri-email").attr("href",$("#field_email_" + parent_key).html());
			    $("a.pri-learn-more").html('Læs mere om ' + $("#title_" + parent_key).html());
		    	$("a.pri-learn-more").attr("href",$("#field_underside_" + parent_key).html());

			    // Hide secondary card
			    $("#SecondaryCard").addClass("hide");

		    }

		    // Fill data secondary card
		    $("div.PolitiDivInfoHeadline").html($("#title_" + selected_key).html());
		    $("span.sec-street").html($("#field_street_" + selected_key).html());
		    $("span.sec-district").html($("#field_city_" + selected_key).html());
		    $("span.sec-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());
		    $("span.sec-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
		    if ($("#field_fax_" + selected_key).html().length <= 0) {
		    	$("span.sec-fax").addClass('hide');
		    }
		    $("span.sec-email").html($("#field_email_" + selected_key).html());
		    $("span.sec-email").attr("attr",$("#field_email_" + selected_key).html());
		     $("a.sec-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.sec-learn-more").attr("href",$("#field_underside_" + selected_key).html());

	    }



	    // Fill data secondary card
	    $("div.PolitiDivInfoHeadline").html($("#title_" + selected_key).html());
	    $("span.sec-street").html($("#field_street_" + selected_key).html());
	    $("span.sec-district").html($("#field_city_" + selected_key).html());
	    $("span.sec-tel").html("Tlf: " + $("#field_phone_" + selected_key).html());
	    // $("span.sec-fax").html("Fax: " + $("#field_fax_" + selected_key).html());
	    $("span.sec-email").html($("#field_email_" + selected_key).html());
	    $("span.sec-email").attr("attr",$("#field_email_" + selected_key).html());
	     $("a.sec-learn-more").html('Læs mere om ' + $("#title_" + selected_key).html());
		    $("a.sec-learn-more").attr("href",$("#field_underside_" + selected_key).html());

	    if ($.isEmptyObject(secondary_card_data)){
	    	$("#SecondaryCard").addClass("hide");
	    } else{
	    	$("#SecondaryCard").removeClass("hide");
	    }
	});

	// map mouse leave
	$(".land").mouseleave(function(){
		$(this).removeClass("selected");
	});

	// Fill data from back end
	for (var i = 1; i < 30; i++) {
		$("#branch" + i).html($("#title_" + i).html());
	}

	var tweet = $(".twitter_widget").html();
	$("#tweetfeed").html(tweet);

});

// end maps feature
