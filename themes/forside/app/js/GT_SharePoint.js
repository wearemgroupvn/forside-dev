/// <reference path="..\js\jquery.d.ts"/>
/// <summary>
/// SharePoint functions
/// </summary>
/// <versions>
///		<version number="1.0.0" date="18-03-2016" author="GT\BH" />
///		<version number="1.0.1" date="29-04-2016" author="GT\BH" comments="Tilfjet til DCA Projektet" />
///		<version number="1.0.1" date="29-04-2016" author="GT\BH" comments="Tilfjet GetItemsRootweb" />
/// </versions>
//#region GT_SharePoint
var GT_SharePoint;
(function (GT_SharePoint) {
    GT_SharePoint.Version = 2013;
    var _clientContext = null;
    GT_SharePoint._spCalls = [];
    GT_SharePoint.initialized = false;

    (function (Operators) {
        Operators[Operators["Equals"] = "Eq"] = "Equals";
        Operators[Operators["GreaterThanOrEquals"] = "Geq"] = "GreaterThanOrEquals";
        Operators[Operators["LessThanOrEquals"] = "Leq"] = "LessThanOrEquals";
    })(GT_SharePoint.Operators || (GT_SharePoint.Operators = {}));
    var Operators = GT_SharePoint.Operators;

    function Initialize() {
        if (!GT_SharePoint.initialized) {
            GT_SharePoint.initialized = true;
            RunCalls();
        }
    }
    GT_SharePoint.Initialize = Initialize;

    function clientContext() {
        if (_clientContext == null) {
            _clientContext = new SP.ClientContext();
        }
        return _clientContext;
    }
    GT_SharePoint.clientContext = clientContext;

    function RunCalls() {
        try  {
            if (GT_SharePoint.initialized) {
                GT_SharePoint._spCalls.forEach(function (call) {
                    call();
                });

                // empty array
                GT_SharePoint._spCalls = [];
            }
        } catch (error) {
        }
    }
    GT_SharePoint.RunCalls = RunCalls;

    function SchemaXml2Json(schemaXml) {
        var jsonObject = {};
        var schemaXmlDoc = $.parseXML(schemaXml);
        $(schemaXmlDoc).find('Field').each(function () {
            $.each(this.attributes, function (i, attr) {
                jsonObject[attr.name] = attr.value;
            });
        });
        $(schemaXmlDoc).find('List').each(function () {
            $.each(this.attributes, function (i, attr) {
                jsonObject[attr.name] = attr.value;
            });
        });
        return jsonObject;
    }
    GT_SharePoint.SchemaXml2Json = SchemaXml2Json;

    function FindField(fieldArray, fieldTitle) {
        var returnValue = null;
        fieldArray.forEach(function (field) {
            if (field.Name == fieldTitle || field.DisplayName == fieldTitle) {
                returnValue = field;
            }
        });

        return returnValue;
    }
    GT_SharePoint.FindField = FindField;

    var PromiseObject = (function () {
        function PromiseObject() {
            this.ExtraContexts = new Array();
            this.Promiser = $.Deferred();
            this.ListItemsCollections = new GT_Core.GenericLists.List();
            this.ListCollection = new GT_Core.GenericLists.List();
            this.TaxonomyCollections = new GT_Core.GenericLists.List();
            this.TestGroups = new GT_Core.GenericLists.List();
            this.AddedItems = new GT_Core.GenericLists.List();
        }
        PromiseObject.prototype.Execute = function () {
            var _this = this;
            this.ExtraContexts.forEach(function (ctx) {
                ctx.executeQueryAsync(Function.createDelegate(_this, _this.ExecuteSucceded), Function.createDelegate(_this, _this.ExecuteFailed));
            });
            clientContext().executeQueryAsync(Function.createDelegate(this, this.ExecuteSucceded), Function.createDelegate(this, this.ExecuteFailed));
        };

        PromiseObject.prototype.ExecuteSucceded = function () {
            var _this = this;
            try  {
                var secondLoad = false;
                var newListItemsCollections = new GT_Core.GenericLists.List();

                this.ListItemsCollections._List.forEach(function (element) {
                    var enumerator = element.Value.getEnumerator();
                    var listItems = new GT_Core.GenericLists.List();
                    while (enumerator.moveNext()) {
                        var currentElement = enumerator.get_current();
                        var values = currentElement.get_fieldValues();
                        listItems.Add(values);
                    }
                    newListItemsCollections.Add(new GT_Core.GenericLists.KeyValuePair(element.Key, listItems));
                });
                this.ListItemsCollections = newListItemsCollections;

                this.TestGroups._List.forEach(function (testGroup) {
                    testGroup.IsMember = SPSecurity.IsUserInGroup(_this.CurrentUser, testGroup.group);
                });

                this.TaxonomyCollections._List.forEach(function (taxColl) {
                    var taxFieldLande = clientContext().castTo(taxColl.Value.Field, SP.Taxonomy.TaxonomyField);
                    var termSet = taxColl.Value.TermStore.getTermSet(taxFieldLande.get_termSetId());
                    taxColl.Value.Terms = termSet.getAllTerms();
                    clientContext().load(taxColl.Value.Terms);
                });

                this.ListCollection._List.forEach(function (list) {
                    list.Value.ContentTypes = list.Value.get_contentTypes();
                    clientContext().load(list.Value.ContentTypes);
                });

                secondLoad = (this.TaxonomyCollections._List.length > 0) || (this.ListCollection._List.length > 0);

                if (secondLoad) {
                    clientContext().executeQueryAsync(Function.createDelegate(this, this.ExecuteTaxonomySucceded), Function.createDelegate(this, this.ExecuteFailed));
                } else {
                    this.Promiser.resolve(this);
                }
            } catch (e) {
            }
        };

        PromiseObject.prototype.ExecuteTaxonomySucceded = function () {
            var newTaxonomyCollections = new GT_Core.GenericLists.List();
            this.TaxonomyCollections._List.forEach(function (taxColl) {
                var termsEnumerator = taxColl.Value.Terms.getEnumerator();
                var listItems = new GT_Core.GenericLists.List();
                while (termsEnumerator.moveNext()) {
                    var currentTerm = termsEnumerator.get_current();
                    var values = new SPTaxonomies.TaxonomyValue(currentTerm);

                    listItems.Add(values);
                }
                newTaxonomyCollections.Add(new GT_Core.GenericLists.KeyValuePair(taxColl.Key, listItems));
            });

            this.TaxonomyCollections = newTaxonomyCollections;

            this.ListCollection._List.forEach(function (list) {
                var ctEnum = list.Value.ContentTypes.getEnumerator();
                var tmpContentTypes = new GT_Core.GenericLists.List();
                while (ctEnum.moveNext()) {
                    var curItem = ctEnum.get_current();
                    var addItem = {};
                    addItem.ID = curItem.get_id();
                    addItem.Title = curItem.get_name();
                    addItem.Description = curItem.get_description();
                    addItem.Hidden = curItem.get_hidden();

                    tmpContentTypes.Add(addItem);
                }
                list.Value.ContentTypes = tmpContentTypes;
            });

            this.Promiser.resolve(this);
        };

        PromiseObject.prototype.ExecuteFailed = function (sender, args) {
            this.Promiser.reject(args.get_message());
        };
        return PromiseObject;
    })();
    GT_SharePoint.PromiseObject = PromiseObject;

    //#region Forms
    (function (Forms) {
        var SPFieldComments = {};

        /// <summary>
        /// Henter kommentarfelter fra en sharepoint form
        /// </summary>
        /// <param name="text" >Teksten der skal ledes efter</param>
        /// <returns>kommentarfeltet</returns>
        /// <usage>GT_SharePoint.Forms.GetCommentContains(<TEXT>)</usage>
        /// </summary>
        function GetCommentContains(text, elementID) {
            var returnValue;
            if (Object.size(SPFieldComments) == 0) {
                //     $("#" + elementID).contents().filter(function () {
                //          return this.nodeType == 8;
                //     })
                $("#" + elementID).find(".ms-formbody").contents().filter(function () {
                    return this.nodeType == 8;
                }).each(function () {
                    var fieldInfo = this.data.split('FieldInternalName="');
                    if (fieldInfo.length > 0) {
                        var fieldName = fieldInfo[1].split('"')[0];
                        SPFieldComments[fieldName] = this;
                        $(this).closest("tr").attr("id", "tr_" + fieldName);
                    }
                });
            }

            returnValue = SPFieldComments[text];
            return returnValue;
        }
        Forms.GetCommentContains = GetCommentContains;

        function FindFormIDFromElement(element) {
            var returnValue = null;
            if (typeof (element.parent().attr("webpartid")) == "undefined") {
                if (element.parent()[0].localName != "body") {
                    returnValue = FindFormIDFromElement(element.parent());
                }
            } else {
                returnValue = element.parent()[0].id;
            }
            return returnValue;
        }
        Forms.FindFormIDFromElement = FindFormIDFromElement;

        function FindRowByComment(element) {
            var returnValue = null;
            if (element.parent()[0].localName != "tr") {
                returnValue = FindRowByComment(element.parent());
            } else {
                returnValue = element.parent();
            }
            return returnValue;
        }
        Forms.FindRowByComment = FindRowByComment;

        var ModalForm = (function () {
            function ModalForm(url, title, onCloseCallback) {
                //Using the DialogOptions class.
                this.options = SP.UI.$create_DialogOptions();
                if (typeof (title) == "undefined") {
                    this.options.title = "My Dialog Title";
                } else {
                    this.options.title = title;
                }
                this.options.width = window.innerWidth;
                this.options.height = window.innerHeight;
                this.options.url = url;
                if (typeof (onCloseCallback) == "undefined") {
                    this.options.dialogReturnValueCallback = function (dialogResult, returnValue) {
                        SP.UI.Notify.addNotification('Something has changed');
                        SP.UI.ModalDialog.RefreshPage(SP.UI.DialogResult.OK);
                    };
                } else {
                    this.options.dialogReturnValueCallback = onCloseCallback;
                }
            }
            ModalForm.prototype.Show = function () {
                SP.UI.ModalDialog.showModalDialog(this.options);
            };
            return ModalForm;
        })();
        Forms.ModalForm = ModalForm;
    })(GT_SharePoint.Forms || (GT_SharePoint.Forms = {}));
    var Forms = GT_SharePoint.Forms;

    //#endregion
    (function (SPWebs) {
        function GetCurrentUser(promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var currentUser = clientContext().get_web().get_currentUser();

            clientContext().load(currentUser);
            promiseObject.CurrentUser = currentUser;

            return promiseObject;
        }
        SPWebs.GetCurrentUser = GetCurrentUser;

        function GetContentTypes(promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var contentTypes = clientContext().get_web().get_availableContentTypes();

            clientContext().load(contentTypes);
            promiseObject.ContentTypes = contentTypes;

            return promiseObject;
        }
        SPWebs.GetContentTypes = GetContentTypes;
    })(GT_SharePoint.SPWebs || (GT_SharePoint.SPWebs = {}));
    var SPWebs = GT_SharePoint.SPWebs;

    (function (SPLists) {
        /// <summary>
        /// Henter items fra list udfra en camlQuerystring
        /// </summary>
        /// <param name="listName" ></param>
        /// <param name="query" ></param>
        /// <param name="success" >Funktion der skal kaldes ved success</param>
        /// <param name="failure" >function der skal kaldes ved fejl</param>
        function GetList(listName, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var oList;

            // Grim implementering
            if (listName.length == "6c0c58ef-e7b4-425e-a1a7-05c99735b535".length) {
                oList = clientContext().get_web().get_lists().getById(listName);
            } else {
                oList = clientContext().get_web().get_lists().getByTitle(listName);
            }
            clientContext().load(oList, 'SchemaXml');
            var oFields = oList.get_fields();
            clientContext().load(oFields);
            promiseObject.list = oList;
            promiseObject.fields = oFields;
            if (GT_SharePoint.Version >= 2013) {
                var oProps = oList.get_rootFolder().get_properties();
                clientContext().load(oProps);
                promiseObject.props = oProps;
            }

            clientContext().executeQueryAsync(Function.createDelegate(promiseObject, function () {
                var schemaXml = this.list.get_schemaXml();
                var listJson = SchemaXml2Json(schemaXml);
                var returnValue = { list: this.list, fields: this.fields, listSchemaXML: listJson };
                var fieldEnumerator = this.fields.getEnumerator();
                var arrFields = [];

                while (fieldEnumerator.moveNext()) {
                    var currentField = fieldEnumerator.get_current();
                    clientContext().load(currentField, 'SchemaXml');
                    arrFields.push(currentField);
                }
                var o2 = { d: this.Promiser, returnValue: returnValue, arrFields: arrFields };
                clientContext().executeQueryAsync(Function.createDelegate(o2, function () {
                    var arrFields = [];
                    this.arrFields.forEach(function (fld) {
                        var schemaXml = fld.get_schemaXml();
                        var fieldJson = SchemaXml2Json(schemaXml);
                        arrFields.push(fieldJson);
                    });
                    this.returnValue.fields = arrFields;
                    this.d.resolve(this.returnValue);
                }), Function.createDelegate(o2, function (sender, args) {
                    this.d.reject(args.get_message());
                }));
                //this.d.resolve(returnValue);
            }), Function.createDelegate(promiseObject, function (sender, args) {
                this.d.reject(args.get_message());
            }));

            return promiseObject;
        }
        SPLists.GetList = GetList;

        function GetItemsFromWebList(listName, query, web, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(query);
            var oList = web.get_lists().getByTitle(listName);
            var oItems = oList.getItems(camlQuery);
            clientContext().load(oItems);
            promiseObject.ListItemsCollections.Add(new GT_Core.GenericLists.KeyValuePair(listName, oItems));

            return promiseObject;
        }
        SPLists.GetItemsFromWebList = GetItemsFromWebList;

        function GetItemsFromWebListCtx(listName, query, web, ctx, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(query);
            var oList = web.get_lists().getByTitle(listName);
            var oItems = oList.getItems(camlQuery);
            ctx.load(oItems);
            promiseObject.ExtraContexts.push(ctx);
            promiseObject.ListItemsCollections.Add(new GT_Core.GenericLists.KeyValuePair(listName, oItems));

            return promiseObject;
        }
        SPLists.GetItemsFromWebListCtx = GetItemsFromWebListCtx;

        function GetItemsFromList(listName, query, promiseObject) {
            promiseObject = GetItemsFromWebList(listName, query, clientContext().get_web(), promiseObject);

            return promiseObject;
        }
        SPLists.GetItemsFromList = GetItemsFromList;

        function GetItemsFromListRootWeb(listName, query, promiseObject) {
            promiseObject = GetItemsFromWebList(listName, query, clientContext().get_site().get_rootWeb(), promiseObject);

            return promiseObject;
        }
        SPLists.GetItemsFromListRootWeb = GetItemsFromListRootWeb;

        function GetItemsFromListUrl(listName, query, siteURL, promiseObject) {
            var ctx = clientContext();
            if (typeof (siteURL) != "undefined") {
                ctx = new SP.ClientContext(siteURL);
            }

            promiseObject = GetItemsFromWebListCtx(listName, query, ctx.get_web(), ctx, promiseObject);

            return promiseObject;
        }
        SPLists.GetItemsFromListUrl = GetItemsFromListUrl;

        function CreateListItem(listName, listItem, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }
            var oList = null;
            if (listName.length == "6c0c58ef-e7b4-425e-a1a7-05c99735b535".length) {
                oList = clientContext().get_web().get_lists().getById(listName);
            } else {
                oList = clientContext().get_web().get_lists().getByTitle(listName);
            }
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var oListItem = oList.addItem(itemCreateInfo);

            for (prop in listItem) {
                oListItem.set_item(prop, listItem[prop]);
            }

            oListItem.update();
            clientContext().load(oListItem);
            promiseObject.AddedItems.Add(oListItem);

            return promiseObject;
        }
        SPLists.CreateListItem = CreateListItem;

        function GetListContentTypes(listName, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var oList = null;
            if (listName.length == "6c0c58ef-e7b4-425e-a1a7-05c99735b535".length) {
                oList = clientContext().get_web().get_lists().getById(listName);
            } else {
                oList = clientContext().get_web().get_lists().getByTitle(listName);
            }

            clientContext().load(oList);
            promiseObject.ListCollection.Add(new GT_Core.GenericLists.KeyValuePair(listName, oList));

            return promiseObject;
        }
        SPLists.GetListContentTypes = GetListContentTypes;
    })(GT_SharePoint.SPLists || (GT_SharePoint.SPLists = {}));
    var SPLists = GT_SharePoint.SPLists;

    (function (SPTaxonomies) {
        var TaxonomyValue = (function () {
            function TaxonomyValue(taxonomy) {
                if (typeof (taxonomy.get_name) == "function") {
                    this.ID = taxonomy.get_id();
                    this.Name = taxonomy.get_name();
                    this.Path = taxonomy.get_pathOfTerm();
                    this.WssId = "";
                } else {
                    this.ID = taxonomy.get_termGuid();
                    this.Name = taxonomy.get_label();
                    this.Path = "";
                    this.WssId = taxonomy.get_wssId();
                }
            }
            return TaxonomyValue;
        })();
        SPTaxonomies.TaxonomyValue = TaxonomyValue;

        var TaxonomyLoad = (function () {
            function TaxonomyLoad() {
            }
            return TaxonomyLoad;
        })();
        SPTaxonomies.TaxonomyLoad = TaxonomyLoad;

        function GetTaxonomiesByField(fieldName, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            var rootWeb = clientContext().get_site().get_rootWeb();
            var taxLoad = new TaxonomyLoad();
            var taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(clientContext());

            taxLoad.Field = rootWeb.get_availableFields().getByInternalNameOrTitle(fieldName);
            taxLoad.TermStore = taxonomySession.getDefaultSiteCollectionTermStore();

            // var taxFieldLande = clientContext().castTo(taxLoad.Field, SP.Taxonomy.TaxonomyField);
            // var termSet = taxLoad.TermStore.getTermSet(taxFieldLande.get_termSetId());
            // taxLoad.Terms = termSet.getAllTerms();
            clientContext().load(taxLoad.Field);
            clientContext().load(taxLoad.TermStore);

            // clientContext().load(taxLoad.Terms);
            promiseObject.TaxonomyCollections.Add(new GT_Core.GenericLists.KeyValuePair(fieldName, taxLoad));

            return promiseObject;
        }
        SPTaxonomies.GetTaxonomiesByField = GetTaxonomiesByField;
    })(GT_SharePoint.SPTaxonomies || (GT_SharePoint.SPTaxonomies = {}));
    var SPTaxonomies = GT_SharePoint.SPTaxonomies;

    (function (SPSecurity) {
        function IsCurrentUserMemberOfGroup(groupName, promiseObject) {
            if (typeof (promiseObject) == "undefined") {
                promiseObject = new GT_SharePoint.PromiseObject();
            }

            promiseObject.CurrentUser = clientContext().get_web().get_currentUser();
            clientContext().load(promiseObject.CurrentUser);
            var allGroups = clientContext().get_web().get_siteGroups();

            //clientContext().load(promiseObject.AllGroups);
            var group = allGroups.getByName(groupName);
            clientContext().load(group);
            var groupUsers = group.get_users();
            clientContext().load(groupUsers);
            promiseObject.TestGroups.Add({ group: group, groupUsers: groupUsers });
            return promiseObject;
        }
        SPSecurity.IsCurrentUserMemberOfGroup = IsCurrentUserMemberOfGroup;

        function IsUserInGroup(user, group) {
            var groupUsers = group.get_users();
            var userInGroup = false;
            var groupUserEnumerator = groupUsers.getEnumerator();
            while (groupUserEnumerator.moveNext()) {
                var groupUser = groupUserEnumerator.get_current();
                if (groupUser.get_id() == user.get_id()) {
                    userInGroup = true;
                    break;
                }
            }
            return userInGroup;
        }
        SPSecurity.IsUserInGroup = IsUserInGroup;
    })(GT_SharePoint.SPSecurity || (GT_SharePoint.SPSecurity = {}));
    var SPSecurity = GT_SharePoint.SPSecurity;

    //#region Lists
    (function (Lists) {
        /// <summary>
        /// Henter items fra list udfra en camlQuerystring
        /// </summary>
        /// <param name="listName" ></param>
        /// <param name="query" ></param>
        /// <param name="success" >Funktion der skal kaldes ved success</param>
        /// <param name="failure" >function der skal kaldes ved fejl</param>
        function GetItemsFromList(listName, query, root, siteURL) {
            var ctx = clientContext();
            if (typeof (siteURL) != "undefined") {
                ctx = new SP.ClientContext(siteURL);
            }
            var d = $.Deferred();
            var oList;
            if (root) {
                oList = ctx.get_site().get_rootWeb().get_lists().getByTitle(listName);
            } else {
                oList = ctx.get_web().get_lists().getByTitle(listName);
            }

            var camlQuery = new SP.CamlQuery();
            camlQuery.set_viewXml(query);
            oItems = oList.getItems(camlQuery);
            ctx.load(oItems);
            var o = { d: d, items: oItems };
            ctx.executeQueryAsync(Function.createDelegate(o, function () {
                this.d.resolve(this.items);
            }), Function.createDelegate(o, function (sender, args) {
                this.d.reject(args.get_message());
            }));

            return d.promise();
        }
        Lists.GetItemsFromList = GetItemsFromList;

        function GetItemFromListByTitle(listName, title, root) {
            var query = '<View><Query><Where><Eq><FieldRef Name=\'Title\'/><Value Type="Text">' + title + '</Value></Eq></Where></Query><RowLimit>1000</RowLimit></View>';
            return GT_SharePoint.Lists.GetItemsFromList(listName, query, root);
        }
        Lists.GetItemFromListByTitle = GetItemFromListByTitle;

        function GetItemFromList(listName, itemId) {
            var d = $.Deferred();
            var oList = clientContext().get_web().get_lists().getByTitle(listName);
            var oItem = oList.getItemById(itemId);
            clientContext().load(oItem);
            var o = { d: d, item: oItem };
            clientContext().executeQueryAsync(Function.createDelegate(o, function () {
                this.d.resolve(this.item);
            }), Function.createDelegate(o, function (sender, args) {
                this.d.reject(args.get_message());
            }));

            return d.promise();
        }
        Lists.GetItemFromList = GetItemFromList;

        function GetList(listName) {
            var d = $.Deferred();
            var oList;

            // Grim implementering
            if (listName.length == "6c0c58ef-e7b4-425e-a1a7-05c99735b535".length) {
                oList = clientContext().get_web().get_lists().getById(listName);
            } else {
                oList = clientContext().get_web().get_lists().getByTitle(listName);
            }
            clientContext().load(oList, 'SchemaXml');
            var oFields = oList.get_fields();
            clientContext().load(oFields);
            var o = { d: d, list: oList, fields: oFields };
            if (GT_SharePoint.Version >= 2013) {
                var oProps = oList.get_rootFolder().get_properties();
                clientContext().load(oProps);
                o.props = oProps;
            }
            clientContext().executeQueryAsync(Function.createDelegate(o, function () {
                var schemaXml = this.list.get_schemaXml();
                var listJson = SchemaXml2Json(schemaXml);
                var returnValue = { list: this.list, fields: this.fields, listSchemaXML: listJson };
                var fieldEnumerator = this.fields.getEnumerator();
                var arrFields = [];

                while (fieldEnumerator.moveNext()) {
                    var currentField = fieldEnumerator.get_current();
                    clientContext().load(currentField, 'SchemaXml');
                    arrFields.push(currentField);
                }
                var o2 = { d: this.d, returnValue: returnValue, arrFields: arrFields };
                clientContext().executeQueryAsync(Function.createDelegate(o2, function () {
                    var arrFields = [];
                    this.arrFields.forEach(function (fld) {
                        var schemaXml = fld.get_schemaXml();
                        var fieldJson = SchemaXml2Json(schemaXml);
                        arrFields.push(fieldJson);
                    });
                    this.returnValue.fields = arrFields;
                    this.d.resolve(this.returnValue);
                }), Function.createDelegate(o2, function (sender, args) {
                    this.d.reject(args.get_message());
                }));
                //this.d.resolve(returnValue);
            }), Function.createDelegate(o, function (sender, args) {
                this.d.reject(args.get_message());
            }));

            return d.promise();
        }
        Lists.GetList = GetList;
    })(GT_SharePoint.Lists || (GT_SharePoint.Lists = {}));
    var Lists = GT_SharePoint.Lists;

    //#endregion
    //#region Configuration
    (function (Configuration) {
        function GetConfiguration(configName) {
            return GT_SharePoint.Lists.GetItemFromListByTitle("Configuration", configName, true);
        }
        Configuration.GetConfiguration = GetConfiguration;

        function CreateConfigurationObject(configObj) {
            var listEnum = configObj.getEnumerator();

            // Vi antager at der kun er ET
            if (listEnum.moveNext()) {
                var currentConf = listEnum.get_current();
                return JSON.parse(currentConf.get_fieldValues().JSON);
            }
            return null;
        }
        Configuration.CreateConfigurationObject = CreateConfigurationObject;
    })(GT_SharePoint.Configuration || (GT_SharePoint.Configuration = {}));
    var Configuration = GT_SharePoint.Configuration;

    //#endregion
    (function (GenericLists) {
        (function (TypeIDs) {
            TypeIDs[TypeIDs["FieldLookupValue"] = "{f1d34cc0-9b50-4a78-be78-d5facfcccfb7}"] = "FieldLookupValue";
            TypeIDs[TypeIDs["FieldURLValue"] = "{fa8b44af-7b43-43f2-904a-bd319497011e}"] = "FieldURLValue";
            TypeIDs[TypeIDs["FieldUserLookupValue"] = "{c956ab54-16bd-4c18-89d2-996f57282a6f}"] = "FieldUserLookupValue";
        })(GenericLists.TypeIDs || (GenericLists.TypeIDs = {}));
        var TypeIDs = GenericLists.TypeIDs;
        var ListItem = (function () {
            function ListItem(listItem) {
                var _this = this;
                this.query = new GT_Core.GenericLists.List();
                this.Fields.forEach(function (field) {
                    if (typeof (listItem[field]) != "undefined") {
                        if (listItem[field] == null) {
                            _this[field] = listItem[field];
                        } else {
                            if (typeof (listItem[field].get_typeId) == "function") {
                                switch (listItem[field].get_typeId()) {
                                    case TypeIDs.FieldLookupValue:
                                        _this[field] = {};
                                        _this[field].ID = listItem[field].get_lookupId();
                                        _this[field].Value = listItem[field].get_lookupValue();
                                        break;

                                    case TypeIDs.FieldURLValue:
                                        _this[field] = {};
                                        _this[field].URL = listItem[field].get_url();
                                        _this[field].Description = listItem[field].get_description();
                                        break;

                                    case TypeIDs.FieldUserLookupValue:
                                        _this[field] = {};
                                        _this[field].ID = listItem[field].get_lookupId();
                                        _this[field].Value = listItem[field].get_lookupValue();
                                        break;

                                    default:
                                        _this[field] = listItem[field];
                                        break;
                                }
                            } else if (typeof (listItem[field].get_childItemType) == "function") {
                                if (listItem[field].get_childItemType().toString().indexOf("TaxonomyFieldValue") > 0) {
                                    _this[field] = [];

                                    listItem[field].get_data().forEach(function (element) {
                                        _this[field].push(new SPTaxonomies.TaxonomyValue(element));
                                    });
                                } else {
                                    _this[field] = listItem[field];
                                }
                            } else if (Object.prototype.toString.call(listItem[field]) === '[object Array]') {
                                _this[field] = [];
                                listItem[field].forEach(function (element) {
                                    if (typeof (element.get_lookupId) == "function") {
                                        var tmp = {};
                                        tmp.ID = element.get_lookupId();
                                        tmp.Value = element.get_lookupValue();
                                        _this[field].push(tmp);
                                    } else {
                                        _this[field].push(element);
                                    }
                                });
                            } else {
                                _this[field] = listItem[field];
                            }
                        }
                    }
                });
            }
            ListItem.prototype.FormatField = function (field) {
                var returnValue = "";
                if (Object.prototype.toString.call(this[field]) === '[object Array]') {
                    this[field].forEach(function (element) {
                        if (typeof (element["Name"]) != "undefined") {
                            returnValue += element["Name"] + ", ";
                        } else if (typeof (element["Value"]) != "undefined") {
                            returnValue += element["Value"] + ", ";
                        }
                    });
                    returnValue = returnValue.substr(0, returnValue.length - 2);
                } else {
                    var element = this[field];
                    if (typeof (element["Name"]) != "undefined") {
                        returnValue += element["Name"] + ", ";
                    } else if (typeof (element["Value"]) != "undefined") {
                        returnValue += element["Value"] + ", ";
                    } else {
                        returnValue = "<h1>test</h1>";
                    }
                }
                return returnValue;
            };

            ListItem.prototype.AddQuery = function (field, value, fieldType, operator) {
                if (typeof (this.query) == "undefined") {
                    this.query = new GT_Core.GenericLists.List();
                }
                this.query.Add(new Query(field, value, fieldType, operator));
            };

            ListItem.prototype.GetQuery = function () {
                var returnValue = "<View><Query><Where>";
                this.query._List.forEach(function (element) {
                    returnValue += element.GetQuery();
                });

                returnValue += "</Where></Query></View>";
                return returnValue;
            };

            ListItem.prototype.ClearQuery = function () {
                this.query = new GT_Core.GenericLists.List();
            };

            ListItem.prototype.AddLastModifiedQuery = function (lastModified) {
                this.AddQuery("Modified", lastModified, new GT_Core.GenericLists.KeyValuePair("DateTime", "IncludeTimeValue"), Operators.GreaterThanOrEquals);
            };

            ListItem.prototype.AllFields = function () {
                var returnValue = '<ViewFields>';
                returnValue += '</ViewFields>';
                return returnValue;
            };

            ListItem.prototype.SelectFields = function () {
                var returnValue = '<ViewFields>';
                this.Fields.forEach(function (field) {
                    returnValue += '<FieldRef Name="' + field + '"/>';
                });
                returnValue += '</ViewFields>';
                return returnValue;
            };
            return ListItem;
        })();
        GenericLists.ListItem = ListItem;

        var Query = (function () {
            function Query(field, value, fieldType, operator) {
                this.Field = field;
                this.FieldType = fieldType;
                this.Value = value;
                this.Operator = operator;
            }
            Query.prototype.GetQuery = function () {
                var returnValue = "";
                returnValue += "<" + this.Operator.toString() + ">";
                returnValue += "<FieldRef Name='" + this.Field + "' />";
                returnValue += "<Value Type='" + this.FieldType.Key + "'>" + this.Value.toString() + "</Value>";
                returnValue += "</" + this.Operator.toString() + ">";
                return returnValue;
            };
            return Query;
        })();
        GenericLists.Query = Query;
    })(GT_SharePoint.GenericLists || (GT_SharePoint.GenericLists = {}));
    var GenericLists = GT_SharePoint.GenericLists;
})(GT_SharePoint || (GT_SharePoint = {}));

//#endregion
ExecuteOrDelayUntilScriptLoaded(GT_SharePoint.Initialize, "sp.js");

// Execute when script has notified loading (Event call)
SP.SOD.executeOrDelayUntilScriptLoaded(GT_SharePoint.Initialize, "sp.js");
SP.SOD.executeFunc('sp.js', 'SP.ClientContext', GT_SharePoint.Initialize);
