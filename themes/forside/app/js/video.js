
document.addEventListener('DOMContentLoaded', function () {
	var mediaElements = document.querySelectorAll('video, audio'), i, total = mediaElements.length;

	for (i = 0; i < total; i++) {
		new MediaElementPlayer(mediaElements[i], {
			pluginPath: '../build/',
		});
	}
});
