/// <reference path="..\..\js\jquery.d.ts"/>
/// <reference path="..\..\globeteam\scripts\GT_Core.ts"/>
/// <reference path="..\..\globeteam\scripts\GT_SharePoint.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var localStore = new GT_Core.LocalStorage.LocalStorage();
var loader = new GT_SharePoint.PromiseObject();

var GT_Organisationskort;
(function (GT_Organisationskort) {
    GT_SharePoint.Version = 2010;
    (function (References) {
        References[References["Kredse"] = "Kredse"] = "Kredse";
    })(GT_Organisationskort.References || (GT_Organisationskort.References = {}));
    var References = GT_Organisationskort.References;

    (function (Browsers) {
        Browsers[Browsers["MSIE"] = "MSIE"] = "MSIE";
        Browsers[Browsers["Chrome"] = "Chrome"] = "Chrome";
        Browsers[Browsers["Other"] = "Other"] = "Other";
    })(GT_Organisationskort.Browsers || (GT_Organisationskort.Browsers = {}));
    var Browsers = GT_Organisationskort.Browsers;

    function DetectBrowser() {
        var returnValue = {};
        var ua = window.navigator.userAgent;

        //IE browser
        if ((ua.indexOf("MSIE ") + ua.indexOf('Trident/') + ua.indexOf('Edge/')) > 0) {
            returnValue.Browser = Browsers.MSIE;

            if (ua.indexOf('Trident/')) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                returnValue.Version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            } else if (ua.indexOf("MSIE ") > 0) {
                // IE 10 or older => return version number
                returnValue.Version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            } else {
                // Edge (IE 12+) => return version number
                returnValue.Version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }
        } else if (ua.indexOf("Chrome/")) {
            returnValue.Browser = Browsers.Chrome;
        }

        return returnValue;
    }
    GT_Organisationskort.DetectBrowser = DetectBrowser;

    function InitializeOLD() {
        //Denne initialisering bruger lazyloading, der ikke er tilladt på anonyme sider.
        GT_Core.WriteDebug("Initialize: Start");

        // Initier localstore
        localStore.Kredse = new GT_Core.LocalStorage.LocalStorageEntry(References.Kredse);
        localStore.Kredse.setLoadBehavior(GT_Core.LocalStorage.LoadBehavior.Allways);
        localStore.Kredse.Loader = function () {
            loader = GT_SharePoint.SPLists.GetItemsFromList(References.Kredse, Kreds.ViewFields(), loader);
        };
        localStore.Kredse.PostLoad = function (result) {
            GT_Core.WriteDebug("Initialize: PostLoad");
            if (this.Load) {
                result.ListItemsCollections._List.forEach(function (listItems) {
                    if (listItems.Key == References.Kredse) {
                        var kredse = new Kredse();
                        listItems.Value._List.forEach(function (listElement) {
                            var addKreds = new GT_Organisationskort.Kreds(listElement);
                            addKreds.SymbolName = "#symbol" + addKreds.ID;

                            kredse.Add(addKreds);
                        });

                        // Ændret da vi ikke viser statsadvokaturen længere
                        // kredse._List.forEach(kreds => {
                        //     kreds.SVG = kreds.GetSubSVG(kredse);
                        // });
                        localStore.Kredse.setValue(kredse._List);
                    }
                });
            }
        };
        if (localStore.Kredse.Load) {
            GT_Core.WriteDebug("Initialize: Load");
            GT_SharePoint._spCalls.push(localStore.Kredse.Loader);
        }

        // Sæt alle kald igang til at køre
        GT_SharePoint._spCalls.push(function () {
            loader.Execute();
        });

        // Kør alle kaldene hvis funktionerne er loadet.
        GT_SharePoint.RunCalls();

        loader.Promiser.promise().done(function (result) {
            GT_Core.WriteDebug("Initialize: done");
            localStore.Kredse.PostLoad(result);
        });
        loader.Promiser.promise().fail(function (result) {
            GT_Core.WriteDebug("Initialize: fail");
            alert("Fail");
        });
    }
    GT_Organisationskort.InitializeOLD = InitializeOLD;

    function Initialize() {
        // Initier localstore
        localStore.Kredse = new GT_Core.LocalStorage.LocalStorageEntry(References.Kredse);
        localStore.Kredse.PostLoad = function (result) {
            GT_Core.WriteDebug("Initialize: PostLoad");
            if (this.Load) {
                // result.ListItemsCollections._List.forEach(listItems => {
                //     if (listItems.Key == References.Kredse) {
                var kredse = new Kredse();
                result.forEach(function (listElement) {
                    var addKreds = new GT_Organisationskort.Kreds(listElement);
                    addKreds.SymbolName = "#symbol" + addKreds.ID;
                    if ((listElement.Parent == "") || (listElement.Parent == "0;#")) {
                        addKreds.Parent = null;
                    } else {
                        addKreds.Parent = {};
                        addKreds.Parent.ID = listElement.Parent.split(";#")[0];
                        addKreds.Parent.Value = listElement.Parent.split(";#")[1];
                    }

                    addKreds.ShowAsSubCard = (listElement.ShowAsSubCard === 'True');
                    addKreds.ShowParentVcard = (listElement.ShowParentVcard === 'True');

                    kredse.Add(addKreds);
                });

                // Ændret da vi ikke viser statsadvokaturen længere
                // kredse._List.forEach(kreds => {
                //     kreds.SVG = kreds.GetSubSVG(kredse);
                // });
                localStore.Kredse.setValue(kredse._List);
                //     }
                // });
            }
        };

        localStore.Kredse.PostLoad(listeKredse);
    }
    GT_Organisationskort.Initialize = Initialize;

    /**
    * Kredse
    */
    var Kredse = (function (_super) {
        __extends(Kredse, _super);
        function Kredse() {
            _super.prototype._List = new Array();
        }
        return Kredse;
    })(GT_Core.GenericLists.List);
    GT_Organisationskort.Kredse = Kredse;

    var Kreds = (function (_super) {
        __extends(Kreds, _super);
        function Kreds(listItem) {
            this.Fields = Kreds.Fields;
            _super.call(this, listItem);
            //GT_Core.WriteDebug(this.ID + " " + this.Title + ": " + this.SVG);
        }
        Kreds.ViewFields = function () {
            var returnValue = '<ViewFields>';
            this.Fields.forEach(function (field) {
                returnValue += '<FieldRef Name="' + field + '"/>';
            });
            returnValue += '</ViewFields>';
            return returnValue;
        };

        Kreds.prototype.GetSubSVG = function (kredse) {
            var returnValue = "";
            var children = kredse.SubList("Parent", this.ID, "ID");

            children.forEach(function (kreds) {
                returnValue += kreds.GetSubSVG(kredse);
            });
            returnValue += this.SVG;

            return returnValue;
        };
        Kreds.Fields = ["ID", "Title", "Parent", "Adresse", "PostAdresse", "Telefon", "Fax", "Email", "Link", "ReadMore", "ShowParentVcard", "ShowAsSubCard", "SVG", "Class", "LIClass", "Highlights"];
        return Kreds;
    })(GT_SharePoint.GenericLists.ListItem);
    GT_Organisationskort.Kreds = Kreds;
})(GT_Organisationskort || (GT_Organisationskort = {}));

(function () {
    GT_Core.WriteDebug("Start");
    GT_Core.LoadJsCssFile("/Scripts/Organisation.css", GT_Core.LoadFileTypes.StyleSheet, true);
    var organisationsKortApp = angular.module('organisationsKortApp', ['ngSanitize']);
    GT_Core.WriteDebug("Før Initialize");
    GT_Organisationskort.Initialize();
    GT_Core.WriteDebug("Efter Initialize");
    organisationsKortApp.controller('organisationsKortAppController', [
        '$http', '$scope', '$sce', function ($http, $scope, $sce) {
            $("#PreInit").hide();
            $("#OrganisationsKort").show();
            var controllerApp = this;
            this.Kredse = new GT_Organisationskort.Kredse();
            this.Kredse.FromObject(localStore.Kredse.getValue());
            this.Message = "Initialiserer";
            this.liste = this.Kredse.SubList();
            this.TreeView = null;
            this.refreshRate = 0;
            this.PrimaryCard = null;
            this.SecondaryCard = null;
            this.CurrentCard = null;

            this.CreateTree = function (parent) {
                var returnValue = [];
                var tmpList = controllerApp.Kredse.SubList("Parent", parent, "ID");
                tmpList.forEach(function (element) {
                    element.Nodes = controllerApp.CreateTree(element.ID);
                    returnValue.push(element);
                });

                return returnValue;
            };

            this.MouseEnter = function (element) {
                var svgElement = "";
                this.CurrentCard = null;

                if (typeof (element.data) != "undefined") {
                    this.CurrentCard = element.data;
                }
                if (typeof (element.kreds) != "undefined") {
                    this.CurrentCard = element.kreds;
                }
                svgElement = this.CurrentCard.SymbolName;
                $("#MapDefinitions").find(".land").attr("class", "land");
                $(".tree").removeClass("OrgChartHover");
                if (GT_Organisationskort.DetectBrowser().Browser == GT_Organisationskort.Browsers.MSIE) {
                    $(svgElement).find("g").toggleClass("selected");
                    $("#MapDefinitions").html($("#MapDefinitions").html());
                } else {
                    $(svgElement).find("g").attr("class", "land selected");
                }

                $("#branch" + this.CurrentCard.ID).toggleClass("OrgChartHover");

                if (this.CurrentCard.ShowParentVcard) {
                    $("#symbol" + this.CurrentCard.Parent.ID).find("g").toggleClass("selectedParent");
                    controllerApp.SecondaryCard = this.CurrentCard;
                    controllerApp.PrimaryCard = controllerApp.Kredse.SubList("ID", this.CurrentCard.Parent.ID)[0];
                } else {
                    if (this.CurrentCard.ShowAsSubCard) {
                        $("#symbol" + this.CurrentCard.Parent.ID).find("g").toggleClass("selectedParent");
                        controllerApp.SecondaryCard = this.CurrentCard;
                        controllerApp.PrimaryCard = null;
                    } else {
                        controllerApp.PrimaryCard = this.CurrentCard;
                        controllerApp.SecondaryCard = null;
                        var subLister = controllerApp.Kredse.SubList("Parent", this.CurrentCard.ID, "ID");
                        $("#MapDefinitions").find(".land").attr("class", "land");
                        if (subLister.length == 0) {
                            $(svgElement).find("g").toggleClass("selected");
                            if (this.CurrentCard.Highlights != null) {
                                if (this.CurrentCard.Highlights == "ALL") {
                                    if (GT_Organisationskort.DetectBrowser().Browser == GT_Organisationskort.Browsers.MSIE) {
                                        $(".land").toggleClass("selected");
                                    } else {
                                        $(".land").attr("class", "land selected");
                                    }
                                    // $(".land").toggleClass("selected");
                                } else {
                                    this.CurrentCard.Highlights.split(";").forEach(function (element) {
                                        svgElement = "#symbol" + element;
                                        $(svgElement).find("g").toggleClass("selected");
                                    });
                                }
                            }
                        } else {
                            controllerApp.ToggleSub(subLister);
                        }
                        $("#MapDefinitions").html($("#MapDefinitions").html());
                    }
                }
            };

            this.ToggleSub = function (subLister) {
                var svgElement = "";

                subLister.forEach(function (subList) {
                    //$("#branch" + subList.ID).toggleClass("OrgChartHover");
                    svgElement = subList.SymbolName;
                    if (GT_Organisationskort.DetectBrowser().Browser == GT_Organisationskort.Browsers.MSIE) {
                        $(svgElement).find("g").toggleClass("selected");
                    } else {
                        $(svgElement).find("g").attr("class", "land selected");
                    }

                    // $(svgElement).find("g").toggleClass("selected")
                    var subSubLister = controllerApp.Kredse.SubList("Parent", subList.ID, "ID");
                    controllerApp.ToggleSub(subSubLister);
                });
            };

            this.MouseLeave = function (element) {
                // var svgElement = "";
                // if (typeof(element.data)!="undefined") {
                //     svgElement =element.data.SymbolName
                // }
                // if (typeof(element.kreds)!="undefined") {
                //     svgElement =element.kreds.SymbolName
                // }
                // $(svgElement).find("g").toggleClass("selected")
            };
            this.MouseOver = function (element) {
                if (typeof (element.data) != "undefined" || typeof (element.kreds) != "undefined") {
                    if (controllerApp.CurrentCard == null) {
                        controllerApp.MouseEnter(element);
                    } else {
                        if (typeof (element.data) != "undefined") {
                            if (element.data.ID != controllerApp.CurrentCard.ID) {
                                controllerApp.MouseEnter(element);
                            }
                        } else {
                            if (element.kreds.ID != controllerApp.CurrentCard.ID) {
                                controllerApp.MouseEnter(element);
                            }
                        }
                    }
                }
            };

            this.Reload = function () {
                // Reload
                // loader = new GT_SharePoint.PromiseObject();
                // if (localStore.Kredse.Load) {
                //     GT_SharePoint._spCalls.push(localStore.Kredse.Loader);
                // }
                // // Sæt alle kald igang til at køre
                // GT_SharePoint._spCalls.push(
                //     function () {
                //         loader.Execute();
                //     });
                // loader.Promiser.promise().done(function (result) {
                //     localStore.Kredse.PostLoad(result);
                controllerApp.Refresh();
                // })
                // // Kør alle kaldene hvis funktionerne er loadet.
                // GT_SharePoint.RunCalls();
            };

            this.Refresh = function () {
                controllerApp.Kredse = new GT_Organisationskort.Kredse();
                controllerApp.Kredse._List = localStore.Kredse.getValue();
                controllerApp.liste = controllerApp.Kredse.SubList();
                controllerApp.Message = "";
                controllerApp.TreeView = controllerApp.CreateTree(null);
                controllerApp.CreateMap();
            };

            this.createImgDefs = function () {
                var returnValue;
                returnValue = $("<svg class='defs-only' xmlns='http://www.w3.org/2000/svg'></svg>");

                controllerApp.Kredse.SubList().forEach(function (kreds) {
                    GT_Core.WriteDebug("----" + kreds.Title + ":" + kreds.SVG);
                    returnValue.append($("<symbol id='symbol" + kreds.ID + "' ></symbol>").append($("<g class='land'  transform='translate(0.000000,2118.000000) scale(0.200000,-0.200000)' ></g>").append($("<path></path>").attr("d", kreds.SVG))));
                });
                returnValue = $("<div></div>").append(returnValue);
                return returnValue;
            };

            this.ShowPostAdresse = function (card) {
                var returnValue = "";
                var cardObject = null;
                if (card == 1) {
                    cardObject = controllerApp.PrimaryCard;
                } else {
                    cardObject = controllerApp.SecondaryCard;
                }

                if (cardObject != null) {
                    returnValue = cardObject.PostAdresse;
                }
                return returnValue;
            };

            this.CreateMap = function () {
                controllerApp.Message = "Creating map";
                $("#MapDefinitions").html(controllerApp.createImgDefs().html());
                controllerApp.Message = "";
            };

            controllerApp.Refresh();
        }]);
})();
