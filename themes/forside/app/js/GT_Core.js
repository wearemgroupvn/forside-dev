/// <reference path="..\js\jquery.d.ts"/>
/// <summary>
/// General functions
/// </summary>
/// <versions>
///		<version number="2.0.0" date="18-03-2016" author="GT\BH" />
///		<version number="2.0.1" date="29-04-2016" author="GT\BH" comments="Tilføjet til DCA Projektet" />
/// </versions>
//#region Extension functions
/// <summary>
/// Returnerer antallet fra et array med navngivne elementer
/// </summary>
/// <param name="obj" >Objektet der skal arbejdes på</param>
/// <returns>Antallet af elementer</returns>
/// <usage>Object.size(ARRAY)</usage>
/// </summary>
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
};

var MultiString = function (f) {
    return f.toString().split('\n').slice(1, -1).join('\n');
};

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}

if (!String.prototype.capitalizeFirstLetter) {
    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };
}

//#endregion
//#region GT_Core
var GT_Core;
(function (GT_Core) {
    (function (LoadFileTypes) {
        LoadFileTypes[LoadFileTypes["JavaScript"] = "js"] = "JavaScript";
        LoadFileTypes[LoadFileTypes["StyleSheet"] = "css"] = "StyleSheet";
    })(GT_Core.LoadFileTypes || (GT_Core.LoadFileTypes = {}));
    var LoadFileTypes = GT_Core.LoadFileTypes;

    function WriteDebug(message, elementID) {
        if (typeof (elementID) == "undefined") {
            elementID = "Debug";
        }
        if (GetParamsFromURL().DisplayDebug) {
            $("#" + elementID).html($("#" + elementID).html() + "<br/>" + message);
        }
    }
    GT_Core.WriteDebug = WriteDebug;

    /// <summary>
    /// Henter parametre fra querystring og lægger dem som properties
    /// </summary>
    /// <returns>Value of key</returns>
    /// <usage>GT_Core.GetParamsFromURL().KEY</usage>
    /// </summary>
    function GetParamsFromURL() {
        var params = {};
        if (location.search) {
            var parts = location.search.substring(1).split('&');
            for (var i = 0; i < parts.length; i++) {
                var nv = parts[i].split('=');
                if (!nv[0])
                    continue;
                params[nv[0]] = nv[1] || true;
            }
        }
        return params;
    }
    GT_Core.GetParamsFromURL = GetParamsFromURL;

    function ShowNotification(text, seconds, notifyarea) {
        if (typeof (notifyarea) == "undefined") {
            notifyarea = "notifications";
        } else {
            if ($("#" + notifyarea).length < 1) {
                notifyarea = "notifications";
            }
        }
        $("#" + notifyarea).text(text);
        if (typeof (seconds) != "undefined") {
            setTimeout(function () {
                $("#" + notifyarea).text("");
            }, seconds * 1000);
        }
    }
    GT_Core.ShowNotification = ShowNotification;

    function LoadJsCssFile(filename, filetype, load, afterload) {
        if (load) {
            var fileref;
            if (filetype == "js") {
                fileref = document.createElement('script');
                fileref.setAttribute("type", "text/javascript");
                fileref.setAttribute("src", filename + "?rev=201602011400");
                if (typeof (afterload) != "undefined") {
                    fileref.onload = afterload;
                }
            } else if (filetype == "css") {
                fileref = document.createElement("link");
                fileref.setAttribute("rel", "stylesheet");
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", filename + "?rev=201602011400");
                var l = document.createElement('link');
                l.rel = 'stylesheet';
                l.href = filename;
                document.getElementsByTagName('head')[0].appendChild(l);
            }
            if (typeof fileref != "undefined") {
                if (document.getElementsByTagName("head")[0].innerHTML.indexOf(filename) < 0) {
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                }
                return fileref;
            }
        } else {
            if (typeof (afterload) != "undefined") {
                afterload();
            }
        }
    }
    GT_Core.LoadJsCssFile = LoadJsCssFile;

    (function (_LocalStorage) {
        (function (LoadBehavior) {
            LoadBehavior[LoadBehavior["Allways"] = "Allways"] = "Allways";
            LoadBehavior[LoadBehavior["Once"] = "Once"] = "Once";
            LoadBehavior[LoadBehavior["Daily"] = "Daily"] = "Daily";
            LoadBehavior[LoadBehavior["Reload"] = "Reload"] = "Reload";
        })(_LocalStorage.LoadBehavior || (_LocalStorage.LoadBehavior = {}));
        var LoadBehavior = _LocalStorage.LoadBehavior;

        var LocalStorage = (function () {
            function LocalStorage() {
                if (GT_Core.GetParamsFromURL().FlushCache) {
                    if (allowLocalStorage) {
                        try  {
                            localStorage.clear();
                        } catch (error) {
                        }
                    }
                }
            }
            return LocalStorage;
        })();
        _LocalStorage.LocalStorage = LocalStorage;

        var LocalStorageEntry = (function () {
            function LocalStorageEntry(name) {
                this.Name = name;
                this.Load = true;
                this.Loader = null;
                this.PostLoad = function () {
                };
                this.LoadBehavior = LoadBehavior.Once;

                if (allowLocalStorage) {
                    if (typeof (localStorage[this.Name]) != "undefined") {
                        this.Value = JSON.parse(localStorage[this.Name]).Value;
                        this.LastLoad = JSON.parse(localStorage[this.Name]).LastLoad;
                    } else {
                        this.Value = null;
                    }
                }

                if (typeof (this.LastLoad) == "undefined") {
                    var date = new Date();

                    this.LastLoad = "{0}-{1}-{2}T{3}:{4}:{5}".format(1900, date.getUTCMonth() + 1, date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                }
            }
            LocalStorageEntry.prototype.setLastLoad = function () {
                var date = new Date();
                this.LastLoad = "{0}-{1}-{2}T{3}:{4}:{5}".format(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes() - 2, date.getUTCSeconds());
            };

            LocalStorageEntry.prototype.setLoadBehavior = function (loadBehavior) {
                if (loadBehavior != LoadBehavior.Reload) {
                    this.LoadBehavior = loadBehavior;
                }

                switch (loadBehavior) {
                    case LoadBehavior.Allways:
                        this.Load = true;
                        break;

                    case LoadBehavior.Once:
                        if (typeof (localStorage[this.Name]) != "undefined") {
                            this.Load = false;
                        }
                        break;
                    case LoadBehavior.Reload:
                        this.Load = true;
                        break;
                    default:
                        break;
                }
            };
            LocalStorageEntry.prototype.setValue = function (value) {
                var saveValue = {
                    Value: value,
                    LastLoad: this.LastLoad
                };
                this.LastLoad = this.LastLoad;
                this.Value = value;
                if (allowLocalStorage) {
                    localStorage[this.Name] = JSON.stringify(saveValue);
                }
            };
            LocalStorageEntry.prototype.getValue = function () {
                var saveValue = [];
                if (allowLocalStorage) {
                    if (typeof (localStorage[this.Name]) != "undefined") {
                        saveValue = JSON.parse(localStorage[this.Name]);
                        this.Value = saveValue.Value;
                        this.LastLoad = saveValue.LastLoad;
                    } else {
                        this.Value = null;
                    }
                }
                return this.Value;
            };
            return LocalStorageEntry;
        })();
        _LocalStorage.LocalStorageEntry = LocalStorageEntry;
        function allowLocalStorage() {
            return (typeof (Storage) !== "undefined");
        }
        _LocalStorage.allowLocalStorage = allowLocalStorage;
    })(GT_Core.LocalStorage || (GT_Core.LocalStorage = {}));
    var LocalStorage = GT_Core.LocalStorage;

    (function (GenericLists) {
        (function (FilterTypes) {
            FilterTypes[FilterTypes["Equals"] = "Equals"] = "Equals";
            FilterTypes[FilterTypes["NotEquals"] = "NotEquals"] = "NotEquals";
            FilterTypes[FilterTypes["Contains"] = "Contains"] = "Contains";
            FilterTypes[FilterTypes["NotContains"] = "NotContains"] = "NotContains";
            FilterTypes[FilterTypes["BeginsWith"] = "BeginsWith"] = "BeginsWith";
            FilterTypes[FilterTypes["EndsWith"] = "EndsWith"] = "EndsWith";
        })(GenericLists.FilterTypes || (GenericLists.FilterTypes = {}));
        var FilterTypes = GenericLists.FilterTypes;

        var KeyValuePair = (function () {
            function KeyValuePair(key, value) {
                this.Key = key;
                this.Value = value;
            }
            return KeyValuePair;
        })();
        GenericLists.KeyValuePair = KeyValuePair;

        var List = (function () {
            function List() {
                this._List = new Array();
                this._ListName = "";
            }
            List.prototype.FromJSON = function (json) {
                var jsonObj = JSON.parse(json);
                for (var propName in jsonObj) {
                    this[propName] = jsonObj[propName];
                }
            };

            List.prototype.FromObject = function (obj) {
                for (var propName in obj) {
                    this[propName] = obj[propName];
                }
            };

            List.prototype.Add = function (element) {
                this._List.push(element);
            };
            List.prototype.AddOrUpdate = function (element, filterField, filterValue, filterSubField, filterType) {
                var item = this.SubList(filterField, filterValue, filterSubField, filterType)[0];
                if (typeof (item) != "undefined") {
                    var index = this._List.indexOf(item);
                    this._List.splice(index, 1);
                }
                this.Add(element);
            };

            List.prototype.AddElement = function (testFieldValue, filterValue, filterType) {
                var returnValue = false;
                switch (filterType) {
                    case FilterTypes.BeginsWith:
                        returnValue = (testFieldValue.indexOf(filterValue) == 0);

                        break;
                    case FilterTypes.Contains:
                        returnValue = (testFieldValue.indexOf(filterValue) >= 0);

                        break;
                    case FilterTypes.NotContains:
                        returnValue = (testFieldValue.indexOf(filterValue) < 0);

                        break;

                    case FilterTypes.NotEquals:
                        returnValue = (testFieldValue != filterValue);

                        break;

                    default:
                        returnValue = (testFieldValue == filterValue);
                        break;
                }
                return returnValue;
            };

            List.prototype.SubList = function (filterField, filterValue, filterSubField, filterType) {
                if (typeof filterType === "undefined") { filterType = FilterTypes.Equals; }
                var returnValue = [];
                var testFieldValue = "";
                var _this = this;

                if (this._List != null) {
                    this._List.forEach(function (element) {
                        if (typeof (filterField) == "undefined") {
                            returnValue.push(element);
                        } else {
                            if (filterValue == null) {
                                if (element[filterField] == null) {
                                    returnValue.push(element);
                                }
                            } else {
                                if (element[filterField] == null) {
                                } else {
                                    testFieldValue = "";
                                    if (typeof (filterSubField) != "undefined") {
                                        if (Object.prototype.toString.call(element[filterField]).indexOf("Array") >= 0) {
                                            element[filterField].forEach(function (testElement) {
                                                if (_this.AddElement(testElement[filterSubField].toString(), filterValue, filterType)) {
                                                    returnValue.push(element);
                                                }
                                            });
                                        } else {
                                            if (_this.AddElement(element[filterField][filterSubField].toString(), filterValue, filterType)) {
                                                returnValue.push(element);
                                            }
                                        }
                                    } else {
                                        if (_this.AddElement(element[filterField].toString(), filterValue, filterType)) {
                                            returnValue.push(element);
                                        }
                                    }
                                }
                            }
                        }
                    });
                }

                return returnValue;
            };
            return List;
        })();
        GenericLists.List = List;
    })(GT_Core.GenericLists || (GT_Core.GenericLists = {}));
    var GenericLists = GT_Core.GenericLists;
})(GT_Core || (GT_Core = {}));
//#endregion
