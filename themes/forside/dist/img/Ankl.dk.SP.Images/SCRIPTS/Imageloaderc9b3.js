﻿var imgCount = 0;
var noOfBullets = 0;
var addImg = function (src, comment, count, noOfImages, ctrlid) {
    
    var li = document.createElement("li");
    var img = document.createElement("img");
    var maxBulletsPart = parseInt($('#slider' + ctrlid + 'maxCount').val());
    //img.setAttribute("src", src);
    //li.appendChild(img);

    //if (typeof li != "undefined")
    //    document.getElementById("slider" + ctrlid + "List").appendChild(li);

    li = document.createElement("li");
    if (typeof li != "undefined") {
        try {
            if (count<maxBulletsPart) {
                document.getElementById("slider" + ctrlid + "BulletsList").appendChild(li);
                noOfBullets = count+1;
            }
        } catch (e) {
            alert(e.message);
        }
    }

    /*
    li = document.createElement("li");
    var comm = document.createElement("p");
    //comm.setAttribute("class", "anklagemyndigheden-Element-P-2");
    var title = comment.split('|')[0];
    comment = comment.split('|')[1];
    if (comment==undefined) {
        comment = '';
    }
    var commentLines = comment.split("<br />");
    for (var i = 0; i < commentLines.length; i++) {
        comm.appendChild(document.createTextNode(commentLines[i]));
        comm.appendChild(document.createElement("br"));
    }
    li.appendChild(comm);
    if (typeof li != "undefined") {
        try {
            document.getElementById("slider" + ctrlid + "CommentsList").appendChild(li);
        } catch (e) {

        }
    }
    */
}

var initializeSlider = function (id, blockWidth, blockHeight) {
    //jQuery(document).ready(function ($) {
    var timerPart = parseInt($('#slider' + id + 'timer').val())*1000;
    var fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", "../_layouts/ankl.dk.sp.images/style/Imageloaderc9b3.css?rev=201609141500");
    if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref);
    }

    try {
        var arrImages = new Array();
        arrImages = imgs.split('¤');
        var arrImagesComments = new Array();
        arrImagesComments = imgComments.split('¤');
        for (var i = 0; i < arrImages.length; i++) {
            if (arrImages[i] != "") {
                addImg(arrImages[i], arrImagesComments[i], i, arrImages.length, id);
            }
        }
    } catch (e) {
        alert(e.message);
    }

    if (timerPart > 0) {
        setInterval(function () {
            moveRight('slider' + id);
        }, timerPart);
    }

    imgCount = $('#slider' + id + ' ul li').length;
    var slideWidth = $('#slider' + id + ' ul li').width();
    var slideHeight = $('#slider' + id + ' ul li').height();
    var sliderUlWidth = imgCount * slideWidth;
    setBullet(0, "slider" + id);
    $("#slider" + id + "Bullets").attr("style", "padding-left: " + Math.round(blockWidth / 2) - (imgCount * 10) + "px");
    $('#slider' + id + '').css({ width: blockWidth, height: blockHeight });

    $('#slider' + id + ' ul').css({ width: sliderUlWidth });

    // Sliderprev
    $('#slider' + id + '>a.control_prev').click(function () {
        moveLeft('slider' + id);
    });

    //Slidernext
    $('#slider' + id + '>a.control_next').click(function () {
        moveRight('slider' + id);
    });

    function moveLeft(id) {
        $('#' + id + ' ul').animate({
            left: +slideWidth
        }, 200, function () {
            $('#' + id + ' ul li:last-child').prependTo('#' + id + ' ul');
            $('#' + id + ' ul').css('left', '');
        });
        $('#' + id + 'Comments ul').animate({
            left: +slideWidth
        }, 200, function () {
            $('#' + id + 'Comments ul li:last-child').prependTo('#' + id + 'Comments ul');
            $('#' + id + 'Comments ul').css('left', '');
        });
        setBullet(-1, id);
    };

    function moveRight(id) {
        $('#' + id + ' ul').animate({
            left: -slideWidth
        }, 200, function () {
            $('#' + id + ' ul li:first-child').appendTo('#' + id + ' ul');
            $('#' + id + ' ul').css('left', '');
        });
        $('#' + id + 'Comments ul').animate({
            left: -slideWidth
        }, 200, function () {
            $('#' + id + 'Comments ul li:first-child').appendTo('#' + id + 'Comments ul');
            $('#' + id + 'Comments ul').css('left', '');
        });
        setBullet(1, id);
    };

    function setBullet(direction, id) {
        curImg = parseInt($('#' + id + 'curImg').val());
        curImg += direction;
        if (curImg > (imgCount - 1)) {
            curImg = 0;
        }
        if (curImg < 0) {
            curImg = imgCount - 1;
        }

        var count = 0;
        var minCount = 0;
        var bulletCount = (Math.ceil(imgCount / noOfBullets));
        var maxCount = bulletCount;
        $('#' + id + 'Bullets ul li').each(function () {
            if ((count * bulletCount) <= curImg && ((count + 1) * bulletCount > curImg)) {
                $(this).attr("class", "selected");
            }
            else {
                $(this).attr("class", "");
            }
            count++;
        });
        $('#' + id + 'curImg').val(curImg);
    }
};