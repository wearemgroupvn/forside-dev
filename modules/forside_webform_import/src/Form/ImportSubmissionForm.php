<?php

namespace Drupal\forside_webform_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;

/**
 * Class ImportSubmissionForm.
 *
 * @package Drupal\forside_webform_import\Form
 *
 * @ingroup forside_webform_import
 */
class ImportSubmissionForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_submission_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // File.
    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Select the import file.'),
      '#required' => FALSE,
    ];

    // Actions.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import file'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();

    // Validate the uploaded file.
    $extensions = ['xlsx xls'];
    $validators = ['file_validate_extensions' => $extensions];
    $file = file_save_upload('file_upload', $validators, FALSE, 0, FILE_EXISTS_RENAME);
    if (isset($file)) {
      if ($file) {
        $form_state->setValue('file_upload', $file);
      }
      else {
        $form_state->setErrorByName('file_upload', $this->t('The import file could not be uploaded.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      module_load_include('inc', 'phpexcel', 'phpexcel');
      // All values from the form.
      $values = $form_state->getValues();
      $file = $form_state->getValue('file_upload');
      $uri = $file->getFileUri();
      $src = drupal_realpath($uri);
      $data = phpexcel_import($src);
      if (!empty($data[0])) {
        $operations = array();
        $batch = array(
          'title' => t('Import submission...'),
          'operations' => array(
            array(
              '\Drupal\forside_webform_import\ImportSubmission::importSubmission',
              array($data[0])
            ),
          ),
          'finished' => '\Drupal\forside_webform_import\ImportSubmission::importFinishedCallback',
        );
        batch_set($batch);
      }
    }
    catch (ImportException $e) {
      drupal_set_message($this->t('Failed to import file due to "%error".', array('%error' => $e->getMessage())));
    }
  }
}
