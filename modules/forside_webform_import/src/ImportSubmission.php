<?php

namespace Drupal\forside_webform_import;

use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformSubmissionForm;

class ImportSubmission {

  public static function importSubmission($values, &$context){
    $message = 'Import submission...';
    $results = array();
    foreach ($values as $item) {
      $report = 0;
      $news = 0;
      $lands = 0;
      if ($item['Nyheds type opmærkning'] == '27;#Årsrapporter, bekendtgørelser og publikationer') {
        $report = 1;
      }
      elseif ($item['Nyheds type opmærkning'] == '61;#Nyheder fra SØIK') {
        $news = 1;
      }
      else {
        $lands = 1;
      }
      $datas = [
        'webform_id' => 'abonnementer',
        'entity_type' => NULL,
        'entity_id' => NULL,
        'in_draft' => FALSE,
        'uid' => '1',
        'langcode' => 'da',
        'token' => 'pgmJREX2l4geg2RGFp0p78Qdfm1ksLxe6IlZ-mN9GZI',
        'uri' => '/webform/abonnementer/api',
        'remote_addr' => '',
        'data' => [
          'landsdaekkende_nyheder_fra_anklagemyndigheden' => $lands,
          'nyheder_fra_soik' => $news,
          'arsrapporter_bekendtgorelser_og_publikationer' => $report,
          'e_mail' => $item['E-mail'],
          'navn' => $item['Fulde navn'],
          'organisation' => $item['Firma'],
        ],
      ];
      // Check webform is open.
      $webform = Webform::load($datas['webform_id']);
      $is_open = WebformSubmissionForm::isOpen($webform);
      if ($is_open === TRUE) {
        $results[] = WebformSubmissionForm::submitValues($datas);
      }
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  function importFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }
}
