jQuery(document).ready(function ($) { 
	
	$('.node-forsite-article-form #edit-field-year option').removeAttr('selected');
	
	$('.node-forsite-article-form #edit-field-year option').each(function(index, el) {
		if($(this).text() == '- None -') {
			$(this).remove();
		}
		if($(this).text() == (new Date()).getFullYear()) {
			$(this).attr("selected","selected");
		}
	});

	var getValueSidebarContent = $('.node-forsite-article-edit-form #edit-field-sidebar-content-0-value').val();
	if(getValueSidebarContent == '') {
		$('.node-forsite-article-edit-form #edit-field-sidebar-content-wrapper').hide();
	}

	$('.node-forsite-article-form #edit-field-sidebar-content-wrapper').hide();

	$('#block-seven-content .admin-list li').each(function(index, el) {
		if($(this).find('.label').text() == 'Simpel side') {
			$(this).find('.label').text('Underside');
		}
	});

	$('.node-forsite-article-form #edit-field-author-or-other-select option').each(function(index, el) {
		if($(this).val() != '' && $(this).val() != 'Mikkel Thastum' && $(this).val() != 'Simon Gosvig' 
			&& $(this).val() != 'Henrik Madsen' && $(this).val() != 'select_or_other') {
			$(this).hide();
		}
	});

	$('.node-forsite-article-edit-form #edit-field-author-or-other-select option').each(function(index, el) {
		if($(this).val() != '' && $(this).val() != 'Mikkel Thastum' && $(this).val() != 'Simon Gosvig' 
			&& $(this).val() != 'Henrik Madsen' && $(this).val() != 'select_or_other') {
			$(this).hide();
		}
	});

	$('.node-forsite-article-edit-form #edit-langcode-0-value option').each(function(index, el) {
		if($(this).val() == 'und' || $(this).val() == 'zxx') {
			$(this).hide();
		}
	});

	$('.node-form #edit-langcode-0-value option').each(function(index, el) {
		if($(this).val() == 'und' || $(this).val() == 'zxx') {
			$(this).hide();
		}
	});

	$('.node-form .js-form-type-language-select label').text('Sprog');
	$('.node-form .js-form-item-title-0-value label').text('Titel');
	// $('.node-form .field--name-field-sidebar-right .paragraphs-dropbutton-wrapper input.field-add-more-submit:not(#edit-field-sidebar-right-1-subform-field-paragraph-type-add-more-add-more-button-videnbasen)').val('Tilføj højrekolonne');
	$('.node-form .field--name-field-sidebar-right .paragraphs-dropbutton-wrapper input#edit-field-sidebar-right-add-more-add-more-button-sidebar-right').val('Tilføj højrekolonne');
});