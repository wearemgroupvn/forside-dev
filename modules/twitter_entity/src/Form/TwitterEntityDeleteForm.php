<?php

namespace Drupal\twitter_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Twitter entity entities.
 *
 * @ingroup twitter_entity
 */
class TwitterEntityDeleteForm extends ContentEntityDeleteForm {


}
